﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tintuc.Common;
using Tintuc.Model;
using Tintuc.Model.Entity;
using Tintuc.Model.Model.User;

namespace Tintuc.Service.User
{
    public class NewsService
    {
        private tintucEntities _dbEntities = new tintucEntities();

        public SearchResultObject<NewsModel> SearchNews(NewsModel model)
        {
            SearchResultObject<NewsModel> searchResult = new SearchResultObject<NewsModel>();
            try
            {
                var listmodel = (from a in _dbEntities.News.AsNoTracking()
                                 join c in _dbEntities.Catageries.AsNoTracking() on a.CatageryId equals c.Id
                                 select new NewsModel()
                                 {
                                     Id = a.Id,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreateDate,
                                     CatageryId = c.Name,
                                     ImagePath = a.ImagePath
                                 }).AsQueryable();


                searchResult.TotalItem = listmodel.Count();
                searchResult.ListResult = listmodel.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ErrorMessage.ERR001, ex.InnerException);
            }
            return searchResult;
        }

        public void CreateNews(NewsModel model)
        {
            using (var trans = _dbEntities.Database.BeginTransaction())
            {
                try
                {

                    Model.Entity.News news = new Model.Entity.News()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Title = model.Title,
                        Description = model.Description,
                        Content = model.Content,
                        CreateBy = model.CreateBy,
                        CreateDate = DateTime.Now,
                        CatageryId = model.CatageryId,
                        ImagePath = model.ImagePath,

                    };

                    _dbEntities.News.Add(news);
                    _dbEntities.SaveChanges();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw new Exception(ErrorMessage.ERR001, ex.InnerException);
                }
            }
        }

        public NewsModel GetNewsById(NewsModel model)
        {
            try
            {
                var news = _dbEntities.News.AsNoTracking().Where(a => a.Id.Equals(model.Id)).Select(a => new NewsModel()
                {
                    Description = a.Description,
                    Title = a.Title,
                    Content = a.Content,
                    CreateDate = a.CreateDate,

                }).FirstOrDefault();

                return news;


            }
            catch (Exception ex)
            {
                throw new Exception(ErrorMessage.ERR001, ex.InnerException);
            }
        }
    }
}
