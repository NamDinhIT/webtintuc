﻿using NTS.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tintuc.Common;
using Tintuc.Model.Entity;
using Tintuc.Model.Model;

namespace Tintuc.Service.User
{
    public class LoginService
    {
        private tintucEntities _dbEntities = new tintucEntities();

        public UserModel Login(LoginModel loginModel)
        {
            if (string.IsNullOrEmpty(loginModel.UserName) || string.IsNullOrEmpty(loginModel.Password))
            {
                throw new BusinessException(ErrorMessage.ERR007, null);
            }

            var userModel = _dbEntities.Users.AsNoTracking().Where(r => r.UserName.ToLower().Equals(loginModel.UserName.ToLower().Trim())).FirstOrDefault();
            if (userModel == null)
            {
                throw new BusinessException(ErrorMessage.ERR008, null);
            }

            if (userModel.IsDisable)
            {
                throw new BusinessException(ErrorMessage.ERR009, null);
            }

            string passwordHash = PasswordUtil.ComputeHash(loginModel.Password + userModel.HassPass);
            if (!userModel.Password.Equals(passwordHash))
            {
                throw new BusinessException(ErrorMessage.ERR005, null);
            }

            //Lấy id tỉnh và huyện cho tài khoản giáo viên theo vùng

            //Lấy thông tin đăng nhập lưu lên cache
            UserModel loginProfileModel = new UserModel();
            loginProfileModel.Id = userModel.Id;
            loginProfileModel.Name = userModel.Name;
            loginProfileModel.UserName = userModel.UserName;
            loginProfileModel.IsDisable = userModel.IsDisable;
            //loginProfileModel.ListRoles = null;
            //Xóa cache
        
            return loginProfileModel;
        }
    }
}
