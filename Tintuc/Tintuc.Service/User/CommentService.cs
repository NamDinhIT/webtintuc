﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tintuc.Common;
using Tintuc.Model;
using Tintuc.Model.Entity;
using Tintuc.Model.Model.User;

namespace Tintuc.Service.User
{
    public class CommentService
    {
        private tintucEntities _dbEntities = new tintucEntities();

        public SearchResultObject<CommentModel> SearchComment(CommentModel model)
        {
            SearchResultObject<CommentModel> searchResult = new SearchResultObject<CommentModel>();
            try
            {
                var listmodel = (from a in _dbEntities.Comments.AsNoTracking()
                                 join b in _dbEntities.Users.AsNoTracking() on a.CreateBy equals b.Id
                                 select new CommentModel()
                                 {
                                     Id = a.Id,
                                     Content = a.Content,
                                     CreateBy = b.Name,
                                     CreateDate = a.CreateDate,
                                 }).AsQueryable();


                searchResult.TotalItem = listmodel.Count();
                searchResult.ListResult = listmodel.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ErrorMessage.ERR001, ex.InnerException);
            }
            return searchResult;
        }

        public void CreateComment(CommentModel model)
        {
            using (var trans = _dbEntities.Database.BeginTransaction())
            {
                try
                {

                    Model.Entity.Comment user = new Model.Entity.Comment()
                    {
                        Id = Guid.NewGuid().ToString(),
                       Content = model.Content,
                       CreateBy = model.CreateBy,
                        CreateDate = DateTime.Now,

                    };
                    _dbEntities.Comments.Add(user);
                    _dbEntities.SaveChanges();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw new Exception(ErrorMessage.ERR001, ex.InnerException);
                }
            }
        }
    }
}
