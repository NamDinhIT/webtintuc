﻿using NTS.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tintuc.Common;
using Tintuc.Model;
using Tintuc.Model.Entity;

namespace Tintuc.Service.Users
{
    public class UserService
    {
        private tintucEntities _dbEntities = new tintucEntities();

        public SearchResultObject<User.UserModel> SearchUser(User.UserModel model)
        {
            SearchResultObject<User.UserModel> searchResult = new SearchResultObject<User.UserModel>();
            try
            {
                var listmodel = (from a in _dbEntities.Users.AsNoTracking()
                                 select new User.UserModel()
                                 {
                                     Id = a.Id,
                                     Name = a.Name,
                                     UserName = a.UserName,
                                     Email = a.Email,
                                     IsDisable = a.IsDisable,
                                     Type = a.Type,
                                 }).AsQueryable();


                searchResult.TotalItem = listmodel.Count();
                searchResult.ListResult = listmodel.ToList();
                searchResult.PathFile = "";
            }
            catch (Exception ex)
            {
                throw new Exception(ErrorMessage.ERR001, ex.InnerException);
            }
            return searchResult;
        }

        public void DeleteUser(string id)
        {
            var check = _dbEntities.Users.FirstOrDefault(u => u.Id.Equals(id));
            using (var trans = _dbEntities.Database.BeginTransaction())
            {
                try
                {
                    _dbEntities.Users.Remove(check);
                    _dbEntities.SaveChanges();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw new Exception(ErrorMessage.ERR001, ex.InnerException);
                }
            }
        }
        public void CreateUser(User.UserModel model)
        {
            using (var trans = _dbEntities.Database.BeginTransaction())
            {
                try
                {
                    string hasspass = PasswordUtil.CreatePasswordHash();

                    Model.Entity.User user = new Model.Entity.User()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Name = model.Name,
                        UserName = model.UserName,
                        Email = model.Email,
                        CreateBy = "1",
                        UpdateBy = "1",
                        IsDisable = true,
                        Type = model.Type,
                        HassPass = hasspass,
                        Password = PasswordUtil.ComputeHash(model.Password + hasspass),
                        CreateDate = DateTime.Now,
                        UpdateDate = DateTime.Now,

                    };
                    _dbEntities.Users.Add(user);
                    _dbEntities.SaveChanges();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw new Exception(ErrorMessage.ERR001, ex.InnerException);
                }
            }
        }
        //public void UpdateSurvey(SurveyModel modelSurvey)
        //{
        //    using (var trans = db.Database.BeginTransaction())
        //    {
        //        try
        //        {
        //            var dateNow = DateTime.Now;
        //            var model = db.Surveys.FirstOrDefault(u => u.Id.Equals(modelSurvey.Id));
        //            if (model == null)
        //            {
        //                throw new Exception("Chủ đề đã bị xóa bởi người dùng khác");
        //            }
        //            model.Name = modelSurvey.Name;
        //            model.OrderNumber = modelSurvey.OrderNumber;
        //            model.IsPublish = modelSurvey.IsPublish;

        //            model.StartDate = DateTimeUtils.ConvertDateFromStr(modelSurvey.StartDate);
        //            model.EndDate = DateTimeUtils.ConvertDateToStr(modelSurvey.EndDate);
        //            model.CreateBy = modelSurvey.CreateBy;
        //            model.UpdateBy = modelSurvey.CreateBy;
        //            model.CreateDate = dateNow;
        //            model.UpdateDate = dateNow;
        //            if (modelSurvey.ListGroupQuestion == null)
        //            {
        //                modelSurvey.ListGroupQuestion = new List<GroupQuestionModel>();
        //            }
        //            foreach (var item in modelSurvey.ListGroupQuestion)
        //            {
        //                if (string.IsNullOrEmpty(item.Id))
        //                {
        //                    item.Id = Guid.NewGuid().ToString();
        //                }
        //                item.SurveyId = model.Id;
        //                if (item.ListQuestion == null)
        //                {
        //                    item.ListQuestion = new List<QuestionsModel>();
        //                }
        //                foreach (var item2 in item.ListQuestion)
        //                {
        //                    if (string.IsNullOrEmpty(item2.Id))
        //                    {
        //                        item2.Id = Guid.NewGuid().ToString();
        //                    }
        //                    item2.GroupQuestionId = item.Id;
        //                }
        //            }
        //            model.ContentResult = JsonConvert.SerializeObject(modelSurvey.ListGroupQuestion);
        //            db.SaveChanges();
        //            trans.Commit();
        //        }
        //        catch (Exception ex)
        //        {
        //            trans.Rollback();
        //            throw new Exception(ErrorMessage.ERR001, ex.InnerException);
        //        }
        //    }
        //}
        //public SurveyModel GetInfo(string id, bool IsGetCache)
        //{
        //    try
        //    {
        //        SurveyModel result = new SurveyModel();
        //        var model = db.Surveys.FirstOrDefault(u => u.Id.Equals(id));
        //        if (model == null)
        //        {
        //            throw new Exception("Chủ đề đã bị xóa bởi người dùng khác");
        //        }
        //        string KeyCache = ConfigurationManager.AppSettings["cacheQuetion"] + ":" + id + ":";
        //        QuestionsModel cacheModel;
        //        result.Id = id;
        //        result.Name = model.Name;
        //        result.ContentResult = model.ContentResult;
        //        result.IsPublish = model.IsPublish;
        //        result.OrderNumber = model.OrderNumber;
        //        result.StartDate = model.StartDate.Value.ToString("dd/MM/yyyy");
        //        result.EndDate = model.EndDate.Value.ToString("dd/MM/yyyy"); ;
        //        result.ListGroupQuestion = JsonConvert.DeserializeObject<List<GroupQuestionModel>>(model.ContentResult);

        //        if (IsGetCache)
        //        {
        //            RedisService<QuestionsModel> redisService = RedisService<QuestionsModel>.GetInstance();
        //            if (result.ListGroupQuestion == null)
        //            {
        //                result.ListGroupQuestion = new List<GroupQuestionModel>();
        //            }
        //            foreach (var item in result.ListGroupQuestion)
        //            {
        //                if (item.ListQuestion == null)
        //                {
        //                    item.ListQuestion = new List<QuestionsModel>();
        //                }
        //                foreach (var item2 in item.ListQuestion)
        //                {
        //                    cacheModel = redisService.Get<QuestionsModel>(KeyCache + item2.Id);
        //                    if (cacheModel != null)
        //                    {
        //                        if (item2.ListAnswer == null)
        //                        {
        //                            item2.ListAnswer = new List<AnswerModel>();
        //                        }
        //                        for (int i = 0; i < item2.ListAnswer.Count; i++)
        //                        {
        //                            try
        //                            {
        //                                item2.ListAnswer[i].CountSelect = cacheModel.ListAnswer[i].CountSelect;
        //                                item2.ListAnswer[i].ListUser = cacheModel.ListAnswer[i].ListUser;
        //                            }
        //                            catch (Exception) { }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        return result;
        //    }
        //    catch (Exception ex)
        //    { throw new Exception(ErrorMessage.ERR001, ex.InnerException); }
        //}
    }
}
