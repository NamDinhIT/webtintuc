﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tintuc.Common;
using Tintuc.Model;
using Tintuc.Model.Entity;
using Tintuc.Model.Model.User;

namespace Tintuc.Service.User
{
    public class CatageryService
    {
        private tintucEntities _dbEntities = new tintucEntities();

        public SearchResultObject<CatageryModel> SearchCatagery(CatageryModel model)
        {
            SearchResultObject<CatageryModel> searchResult = new SearchResultObject<CatageryModel>();
            try
            {
                var listmodel = (from a in _dbEntities.Catageries.AsNoTracking()
                                 select new CatageryModel()
                                 {
                                     Id = a.Id,
                                     Name = a.Name,
                                 }).AsQueryable();


                searchResult.TotalItem = listmodel.Count();
                searchResult.ListResult = listmodel.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ErrorMessage.ERR001, ex.InnerException);
            }
            return searchResult;
        }

        public void CreateComment(CatageryModel model)
        {
            using (var trans = _dbEntities.Database.BeginTransaction())
            {
                try
                {

                    Model.Entity.Catagery catagery = new Model.Entity.Catagery()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Name = model.Name

                    };
                    _dbEntities.Catageries.Add(catagery);
                    _dbEntities.SaveChanges();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw new Exception(ErrorMessage.ERR001, ex.InnerException);
                }
            }
        }
    }
}
