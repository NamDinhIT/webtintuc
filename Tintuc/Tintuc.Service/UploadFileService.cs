﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using Tintuc.Model.Model.User;

namespace Tintuc.Service
{
    public class UploadFileService
    {
        public static UploadFileModel UploadFile(HttpPostedFile file, string folderName)
        {
            var fileArray = file.FileName.ToString().Split('.');
            string fileName = Guid.NewGuid().ToString() + "." + fileArray[fileArray.Length - 1];
            #region[ảnh thường]
            UploadFileModel imageResult = new UploadFileModel();
            string pathFolder = "FileUpload/" + folderName;
            string pathFolderServer = HostingEnvironment.MapPath("~/" + pathFolder);
            string fileResult = string.Empty;
            // Kiểm tra folder là tên của ProjectId đã tồn tại chưa.
            if (!Directory.Exists(pathFolderServer))
            {
                Directory.CreateDirectory(pathFolderServer);
            }
            // kiểm tra size file > 0
            if (file.ContentLength > 0)
            {
                file.SaveAs(pathFolderServer + fileName);
                fileResult =pathFolder + fileName;

            }
            #endregion
            //Image image = Image.FromFile(pathFolderServer + fileName);
            //Image thumb = image.GetThumbnailImage(120, 120, () => false, IntPtr.Zero);
            //thumb.Save(Path.ChangeExtension(pathFolderServer + fileName, "thumb"));
            imageResult.FileSize = file.ContentLength;
            imageResult.FileUrl = fileResult;
            imageResult.FileUrlThum = fileResult;
            imageResult.FileName = file.FileName;
            imageResult.FullFileUrl = pathFolderServer + fileName;
            return imageResult;
        }

        public static void DeleteFile(string fileName)
        {
            // Xóa folder chứa file ảnh đại diện cũ
            if (!string.IsNullOrEmpty(fileName))
            {
                string path = HostingEnvironment.MapPath("~/" + fileName);
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
            }
        }

    }
}
