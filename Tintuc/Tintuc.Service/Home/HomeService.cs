﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tintuc.Model;
using Tintuc.Model.Entity;
using Tintuc.Model.Model.Home;
using Tintuc.Model.Model.User;

namespace Tintuc.Service.Home
{
    public class HomeService
    {
        private tintucEntities _dbEntities = new tintucEntities();

        public SearchResultObject<HomeModel> SearchHome(CatageryModel model)
        {
            SearchResultObject<HomeModel> searchResult = new SearchResultObject<HomeModel>();
            var data = _dbEntities.Catageries.AsNoTracking().Select(a => new HomeModel { Id = a.Id, CatageryName = a.Name }).AsQueryable();

            if (string.IsNullOrEmpty(model.Id))
            {
                data = data.Where(a => a.Id.Equals(model.Id)).AsQueryable();
            }

            foreach (var item in data.ToList())
            {
                item.ListNews = _dbEntities.News.AsNoTracking().Select(b => new Model.Model.User.NewsModel
                {
                    Id = b.Id,
                    Title = b.Title,
                    Description = b.Description,
                    Content = b.Content,
                    CreateDate = b.CreateDate,
                }).ToList();
            }



            searchResult.ListResult = data.ToList();
            return searchResult;
        }

    }
}
