﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="NewsDetail.aspx.cs" Inherits="Tintuc.NewsDetail" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="ScriptController/newsdetail.js"></script>
    <div class="container" style="margin-top: 50px">
        <div class="row">
            <h2 class="title" id="title">

            </h2>
            <p class="createdate" id="createdate">

            </p>
            <p class="description" id="description">

            </p>
            <div class="contentbody" id="contentbody">

            </div>
        </div>
    </div>
</asp:Content>
