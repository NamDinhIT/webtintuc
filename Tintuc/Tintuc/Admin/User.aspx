﻿<%@ Page Language="C#" MasterPageFile="~/LayoutAdmin.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="Tintuc.Admin.User" %>

<asp:Content runat="server" ID="content" ContentPlaceHolderID="ContentPlaceHolder1">
    <script src="../Scripts/jquery-3.4.1.min.js"></script>
    <script src="../ScriptController/User.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <table class="table">
        <thead>
            <tr>
                <th scope="col">STT</th>
                <th scope="col">Họ và tên</th>
                <th scope="col">Tên tài khoản</th>
                <th scope="col">Email</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</asp:Content>
