﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LayoutAdmin.Master" AutoEventWireup="true" CodeBehind="News.aspx.cs" Inherits="Tintuc.Admin.News" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../Scripts/jquery-3.4.1.min.js"></script>
    <script src="../ScriptController/News.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <div class="row">
        <div class="col-md-6 text-left">
            <label class="form-label" style="line-height: 35px;">
                Tổng số
                       
                    <span class="bold text-danger" id="total"></span> bài đăng</label>
        </div>

        <div class="col-md-6 text-right">
            <button type="button" class="btn btn-success" onclick="createUrl()" container="body">
                <i class="fa fa-plus"></i>
                Thêm mới
            </button>
        </div>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">STT</th>
                <%--<th scope="col">Ảnh</th>--%>
                <th scope="col">Tên bài</th>
                <th scope="col">Mô tả</th>
                <th scope="col">Ngày đăng</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</asp:Content>
