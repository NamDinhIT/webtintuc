﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/LayoutAdmin.Master" CodeBehind="Comment.aspx.cs" Inherits="Tintuc.Admin.Comment" %>

<asp:Content runat="server" ID="content" ContentPlaceHolderId="ContentPlaceHolder1">
     <script src="../Scripts/jquery-3.4.1.min.js"></script>
    <script src="../ScriptController/Comment.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <table class="table">
        <thead>
            <tr>
                <th scope="col">STT</th>
                <th scope="col">Nội dung</th>
                <th scope="col">Người bình luận</th>
                <th scope="col">Ngày bình luận</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</asp:Content>