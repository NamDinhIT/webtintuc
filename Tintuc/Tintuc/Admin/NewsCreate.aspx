﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LayoutAdmin.Master" AutoEventWireup="true" CodeBehind="NewsCreate.aspx.cs" Inherits="Tintuc.Admin.NewsCreate" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../Scripts/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.tiny.cloud/1/9b8hkhsg3rejc2t8ekt29ecrjpsb23myusqr6jb4f8fnunfx/tinymce/5/tinymce.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="../ScriptController/NewsCreate.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
            <div class="form-group">
                <label class="form-label">
                    Danh mục
                    <span class='text-danger text-strong'>*</span>
                </label>
                <div class="controls" onchange="ChangeCatagery()">
                    <select id="catagery" name="catagery" class="form-control" onchange="ChangeCatagery()">
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="form-label">
                    Tiêu đề
                           
                    <span class='text-danger text-strong'>*</span>
                </label>
                <div class="controls">
                    <input type="text" class="form-control" name="title" id="title"
                        maxlength="300" required>
                </div>
            </div>
            <div class="form-group">
                <label class="form-label">
                    Mô tả
                           
                    <span class='text-danger text-strong'>*</span>
                </label>
                <div class="controls">
                    <textarea type="text" class="form-control" name="Description"
                        id="description" maxlength="300" required>
                            </textarea>
                </div>
            </div>
            <label class="form-label">Nội dung</label>
            <textarea id="basic-example">
</textarea>
        </div>
    </div>
    <div class="content-body  padding-15 text-right">
        <button type="button" class="btn btn-success" onclick="save()">
            <i class="fa fa-save"></i>&nbsp;
                <span>Lưu</span>
        </button>
        &nbsp;

            <button type="button" class="btn btn-danger" onclick="Close()">
                <i class="fa fa-power-off"></i>&nbsp;
                <span>Đóng
                </span>
            </button>
    </div>
</asp:Content>
