﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Tintuc.Model.Model.User;
using Tintuc.Service.User;

namespace Tintuc.Controllers
{
    [RoutePrefix("Admin/api/comment")]

    public class CommentController : ApiController
    {
        private CommentService comment = new CommentService();

        [Route("search")]
        [HttpPost]
        public HttpResponseMessage Comment(CommentModel CommentModel)
        {
            try
            {
                var commentData = comment.SearchComment(CommentModel);
                return Request.CreateResponse(HttpStatusCode.OK, commentData);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
       
    }
}
