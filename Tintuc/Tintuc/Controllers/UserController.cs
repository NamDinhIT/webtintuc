﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Tintuc.Service.User;
using Tintuc.Service.Users;

namespace Tintuc.Controllers
{
    [RoutePrefix("Admin/api/user")]
    public class UserController : ApiController
    {
        private UserService users = new UserService();

        [Route("search")]
        [HttpPost]
        public HttpResponseMessage Login(UserModel model)
        {
            try
            {
                var data = users.SearchUser(model);
                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
