﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Tintuc.Model.Model.User;
using Tintuc.Service.User;

namespace Tintuc.Controllers
{
    [RoutePrefix("Admin/api/catagery")]
    public class CatageryController : ApiController
    {
        private CatageryService catageryService = new CatageryService();

        [Route("search")]
        [HttpPost]
        public HttpResponseMessage Comment(CatageryModel model)
        {
            try
            {
                var commentData = catageryService.SearchCatagery(model);
                return Request.CreateResponse(HttpStatusCode.OK, commentData);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("create")]
        [HttpPost]
        public HttpResponseMessage Create(CatageryModel model)
        {
            try
            {
                catageryService.CreateComment(model);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
