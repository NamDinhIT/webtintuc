﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Tintuc.Model.Model;
using Tintuc.Service.User;

namespace Tintuc.Controllers
{
    [RoutePrefix("Admin/api/Authorize")]
    public class LoginController : ApiController
    {
        private LoginService authorizeBusiness = new LoginService();

        [Route("Login")]
        [HttpPost]
        public HttpResponseMessage Login(LoginModel loginModel)
        {
            try
            {
                UserModel loginProfileModel = authorizeBusiness.Login(loginModel);
                return Request.CreateResponse(HttpStatusCode.OK, loginProfileModel);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
