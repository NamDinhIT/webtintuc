﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Tintuc.Model.Model.User;
using Tintuc.Service.Home;

namespace Tintuc.Controllers
{
    [RoutePrefix("Admin/api/home")]

    public class HomeController : ApiController
    {
        private HomeService catageryService = new HomeService();

        [Route("search")]
        [HttpPost]
        public HttpResponseMessage Home(CatageryModel model)
        {
            try
            {
                var commentData = catageryService.SearchHome(model);
                return Request.CreateResponse(HttpStatusCode.OK, commentData);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
