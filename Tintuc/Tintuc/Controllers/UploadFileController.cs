﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Tintuc.Model.Model.User;
using Tintuc.Service;

namespace Tintuc.Controllers
{
    [RoutePrefix("Admin/api/upload")]
    public class UploadFileController : ApiController
    {
        [Route("UploadFile")]
        [HttpPost]
        public HttpResponseMessage UploadFile()
        {
            var dateNow = DateTime.Now;
            UploadFileModel imageResult = null;
            try
            {
                string fileFolder = HttpContext.Current.Request.Form["filefolder"];
                //var modelJson = HttpContext.Current.Request.Form["Model"];
                //var model = JsonConvert.DeserializeObject<UploadModel>(modelJson);
                //if (!model.KeyAuthorize.Equals(keyAuthorize))
                //{
                //    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Không có quyền sử dụng chức năng này!");
                //}
                HttpFileCollection hfc = HttpContext.Current.Request.Files;
                if (hfc.Count > 0)
                {
                    imageResult = UploadFileService.UploadFile(hfc[0], fileFolder);
                }
                return Request.CreateResponse(HttpStatusCode.OK, imageResult);
            }
            catch (Exception ex)
            {
                //Xóa file trên server nếu cập nhật lỗi
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("UploadListFile")]
        [HttpPost]
        public HttpResponseMessage UploadListFile()
        {
            var dateNow = DateTime.Now;
            List<UploadFileModel> lstResult = new List<UploadFileModel>();
            try
            {
                string fileFolder = HttpContext.Current.Request.Form["filefolder"];
                //if (!model.KeyAuthorize.Equals(keyAuthorize))
                //{
                //    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Không có quyền sử dụng chức năng này!");
                //}
                HttpFileCollection hfc = HttpContext.Current.Request.Files;
                if (hfc.Count > 0)
                {
                    for (int i = 0; i < hfc.Count; i++)
                    {
                        // Upload file lên server
                        var rs = UploadFileService.UploadFile(hfc[i], fileFolder);
                        lstResult.Add(rs);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, lstResult);
            }
            catch (Exception ex)
            {
                //Xóa file trên server nếu cập nhật lỗi
                foreach (var item in lstResult)
                {
                    UploadFileService.DeleteFile(item.FileUrl);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
