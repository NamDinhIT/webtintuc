﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Tintuc.Model.Model.User;
using Tintuc.Service.User;

namespace Tintuc.Controllers
{
    [RoutePrefix("Admin/api/news")]

    public class NewsController : ApiController
    {
        private NewsService newsService = new NewsService();

        [Route("search")]
        [HttpPost]
        public HttpResponseMessage News(NewsModel model)
        {
            try
            {
                var commentData = newsService.SearchNews(model);
                return Request.CreateResponse(HttpStatusCode.OK, commentData);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("create")]
        [HttpPost]
        public HttpResponseMessage create(NewsModel model)
        {
            try
            {
                newsService.CreateNews(model);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [Route("getbyid")]
        [HttpPost]
        public HttpResponseMessage GetNewsById(NewsModel model)
        {
            try
            {
                var commentData = newsService.GetNewsById(model);
                return Request.CreateResponse(HttpStatusCode.OK, commentData);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
