﻿
var model = {
    Id: ''
}

var full_url = document.URL; // Get current url
var url_array = full_url.split('=') // Split the string into an array with / as separator
this.model.Id = url_array[url_array.length - 1];  

$.post("Admin/api/news/getbyid", model)
    .done(function (result) {
        if (result) {
            document.getElementById("title").innerHTML = result.Title;
            document.getElementById("description").innerHTML = result.Description;
            document.getElementById("createdate").innerHTML = result.CreateDate;
            document.getElementById("contentbody").innerHTML = result.Content;
        }
    })
    .fail(function (xhr, status, error) {
        alert("Có lỗi trong quá trình xử lý!");
    });