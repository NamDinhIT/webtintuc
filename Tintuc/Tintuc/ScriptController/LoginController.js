﻿

function Login() {
    var modelLogin = {
        UserName: $("#username").val(),
        Password: $("#password").val()
    };

    $.post("api/Authorize/Login", modelLogin, function (result) {
        if (result) {
            localStorage.setItem('currentItem', JSON.stringify(result));
            window.location.href = "/Admin/User.aspx";
        }
        else {
            toastr.error(result.Message, { timeOut: 500 });
        }
    });
}
