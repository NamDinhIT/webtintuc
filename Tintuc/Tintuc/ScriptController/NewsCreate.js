﻿
let user = JSON.parse(localStorage.getItem('currentItem'));
if (user == null) {
    window.location.href = '/Admin/Login.html';

}

tinymce.init({
    selector: 'textarea#basic-example',
    height: 500,
    menubar: false,
    plugins: [
        "advlist autolink link image lists charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
        "save table contextmenu directionality emoticons template paste textcolor"
    ],

    /* toolbar */
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",

    /* style */
    style_formats: [
        {
            title: "Headers", items: [
                { title: "Header 1", format: "h1" },
                { title: "Header 2", format: "h2" },
                { title: "Header 3", format: "h3" },
                { title: "Header 4", format: "h4" },
                { title: "Header 5", format: "h5" },
                { title: "Header 6", format: "h6" }
            ]
        },
        {
            title: "Inline", items: [
                { title: "Bold", icon: "bold", format: "bold" },
                { title: "Italic", icon: "italic", format: "italic" },
                { title: "Underline", icon: "underline", format: "underline" },
                { title: "Strikethrough", icon: "strikethrough", format: "strikethrough" },
                { title: "Superscript", icon: "superscript", format: "superscript" },
                { title: "Subscript", icon: "subscript", format: "subscript" },
                { title: "Code", icon: "code", format: "code" }
            ]
        },
        {
            title: "Blocks", items: [
                { title: "Paragraph", format: "p" },
                { title: "Blockquote", format: "blockquote" },
                { title: "Div", format: "div" },
                { title: "Pre", format: "pre" }
            ]
        },
        {
            title: "Alignment", items: [
                { title: "Left", icon: "alignleft", format: "alignleft" },
                { title: "Center", icon: "aligncenter", format: "aligncenter" },
                { title: "Right", icon: "alignright", format: "alignright" },
                { title: "Justify", icon: "alignjustify", format: "alignjustify" }
            ]
        }
    ]
});



function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#imageUpload").change(function () {
    readURL(this);
});

function Close() {
    window.location.href = '/Admin/News.aspx';
}

model = {
    Title: '',
    Description: '',
    CatageryId: '',
    CreateBy: '',
    ImagePath: '',
    Content: '',
    CreateBy:''
}

var catageryId = '';

function ChangeCatagery() {
    var selectedCountry = $('#catagery').val();
    catageryId = selectedCountry
}

function onFileChange() {
    var fd = new FormData();
    var files = $('#file')[0].files;

    // Check file selected or not
    if (files.length > 0) {
        fd.append('file', files[0]);
        $.ajax({
            url: 'api/upload/UploadFile',
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response != 0) {
                    $("#img").attr("src", response.FileUrl);
                    $(".preview img").show(); // Display image element
                } else {
                    alert('file not uploaded');
                }
            },
        });
    } else {
        alert("Please select a file.");
    }


}


function save() {
    this.ChangeCatagery();
    this.model.Title = document.getElementById('title').value;
    this.model.Description = document.getElementById('description').value;
    this.model.CatageryId = this.catageryId;
    this.model.Content = tinymce.get("basic-example").getContent();
    this.model.CreateBy = JSON.parse(localStorage.getItem('currentItem')).Id;

    $.post("api/news/create", model )
        .done(function (result) {
            if (result) {
                alert("Thêm mới tin tức thành công!");
                this.Close();
            }
        })

        .fail(function (xhr, status, error) {
            alert("Có lỗi trong quá trình xử lý!");
        });
}

$.post("api/catagery/search")
    .done(function (result) {
        if (result) {

            var json = result.ListResult;

            json.map(x => ($("select").append(`<option value="${x.Id}">${x.Name}</option>`)));
        }
    })
    .fail(function (xhr, status, error) {
        alert("Có lỗi trong quá trình xử lý!");
    });
