﻿
var modelSearch =
{
    Name: ''
};

var model = {
    Id:''
}

$.post("Admin/api/news/search", modelSearch)
    .done(function (result) {
        if (result) {
            var json = result.ListResult;

            json.map(x => ($("#content").append(`<div style="padding-bottom: 10px; height: 100px;"> <a href="/NewsDetail.aspx?id=${x.Id}"">${x.Title} </a> <p>${x.CreateDate}</p> <p>${x.Description}</p> <div> <hr>`)));

        }
    })
    .fail(function (xhr, status, error) {
        alert("Có lỗi trong quá trình xử lý!");
    });

function showNewsDetail(id) {
    this.model.Id = id;
    $.post("Admin/api/news/getbyid", model)
        .done(function (result) {
            if (result) {
                document.getElementById("title").innerHTML = result.Title;
                document.getElementById("description").innerHTML = result.Description;
                document.getElementById("createdate").innerHTML = result.CreateDate;
                document.getElementById("contentbody").innerHTML = result.Content;
            }
        })
        .fail(function (xhr, status, error) {
            alert("Có lỗi trong quá trình xử lý!");
        });
}
