﻿let user = JSON.parse(localStorage.getItem('currentItem'));
if (user == null) {
    window.location.href = '/Admin/Login.html';

}

var modelSearch =
{
    Name: ''
};

$.post("api/user/search", modelSearch)
    .done(function (result) {
        if (result) {
            var json = result.ListResult;

            for (var i = 0; i < json.length; i++) {
                j = i + 1;
                tr = $('<tr/>');
                tr.append("<td>" + j  + "</td>");
                tr.append("<td>" + json[i].Name + "</td>");
                tr.append("<td>" + json[i].UserName + "</td>");
                tr.append("<td>" + json[i].Email + "</td>");
                $('table').append(tr);
            } 
        }
    })
    .fail(function (xhr, status, error) {
        $('#loader_id').removeClass("loader");
        document.getElementById("overlay").style.display = "none";
    });
