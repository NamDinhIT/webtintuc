﻿let user = JSON.parse(localStorage.getItem('currentItem'));
if (user == null) {
    window.location.href = '/Admin/Login.html';

}

var modelSearch =
{
    Name: ''
};

$.post("api/comment/search", modelSearch)
    .done(function (result) {
        if (result) {
            var json = result.ListResult;

            for (var i = 0; i < json.length; i++) {
                j = i + 1;
                tr = $('<tr/>');
                tr.append("<td>" + j + "</td>");
                tr.append("<td>" + json[i].Content + "</td>");
                tr.append("<td>" + json[i].CreateBy + "</td>");
                tr.append("<td>" + json[i].CreateDate + "</td>");
                $('table').append(tr);
            }
        }
    })
    .fail(function (xhr, status, error) {
        alert("Có lỗi trong quá trình xử lý!");
    });
