﻿import { localstorage } from "modernizr";

function logout() {
    localstorage.removeItem('currentUser');
}