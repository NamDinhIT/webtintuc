﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tintuc.Model.Model.User;

namespace Tintuc.Model.Model.Home
{
    public class HomeModel
    {
        public string Id { get; set; }
        public string CatageryName { get; set; }
        public List<NewsModel> ListNews { get; set; }
    }
}
