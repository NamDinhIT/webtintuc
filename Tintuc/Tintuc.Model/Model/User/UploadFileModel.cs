﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tintuc.Model.Model.User
{
    public class UploadFileModel
    {
        public string FileUrlThum { get; set; }
        public string FileUrl { get; set; }
        public decimal FileSize { get; set; }
        public string FileName { get; set; }
        public string FullFileUrl { get; set; }
    }
}
