﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tintuc.Model.Model.User
{
    public class NewsModel
    {
        public string Id { get; set; }
        public string CatageryId { get; set; }
        public string Title { get; set; }
        public string ImagePath { get; set; }
        public string FileName { get; set; }
        public string FileUrl { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }

    }
}
