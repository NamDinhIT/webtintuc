﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tintuc.Model;

namespace Tintuc.Service.User
{
    public class UserModel : SearchConditionBase
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public int Type { get; set; }
        public string HassPass { get; set; }
        public string Password { get; set; }
        public System.DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string CreateBy { get; set; }
        public string UpdateBy { get; set; }
        public bool IsDisable { get; set; }
    }
}
