﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tintuc.Model.Model.User
{
    public class CommentModel
    {
        public string Id { get; set; }
        public string Content { get; set; }
        public string CreateBy { get; set; }
        public System.DateTime CreateDate { get; set; }
    }
}
